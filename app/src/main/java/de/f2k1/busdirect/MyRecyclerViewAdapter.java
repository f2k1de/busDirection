package de.f2k1.busdirect;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import static java.lang.Math.round;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<PositionsData> mData;
    private List<AnzeigenData> aData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    MyRecyclerViewAdapter(Context context, List<PositionsData> data, List<AnzeigenData> anzeigenData) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.aData = anzeigenData;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        BusDetails busDetails = new BusDetails();

        Location to = new Location("");
        to.setLatitude(mData.get(i).getLat());
        to.setLongitude(mData.get(i).getLon());
        float bearing = MainActivity.location.bearingTo(to);
        String text = "";
        for(int j = 0; j < aData.size(); j++) {
            if(aData.get(j).getId().equals(mData.get(i).getId())) {
                text = text + aData.get(j).getLinie().trim() + " " + aData.get(j).getZiel().trim();
                break;
            }
        }
        /*
        if(!busDetails.isStadtbus(mData.get(i).getId())) {
            holder.tvBusName.setTypeface(null, Typeface.ITALIC);
        }*/
        holder.tvBusName.setText(text);
        holder.tvSpeed.setText(mData.get(i).getSpeed() + " km/h");
        holder.tvDistance.setText((round(mData.get(i).getDistance() * 100)) / 100 + " m");
        holder.tvBusId.setText(mData.get(i).getId());
        holder.imageView.setRotation(bearing);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvBusName, tvDistance, tvSpeed, tvBusId;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            tvBusName = itemView.findViewById(R.id.tvBusName);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            imageView = itemView.findViewById(R.id.imageView);
            tvSpeed = itemView.findViewById(R.id.tvSpeed);
            tvBusId = itemView.findViewById(R.id.tvBusId);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id).getId();
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}