package de.f2k1.busdirect;

class PositionsData {
    private String id;
    private double lon;
    private double lat;
    private int dir;
    private double speed;
    private long timestamp;
    private double meters;
    PositionsData(String pId, double pLon, double pLat, int pDir, double pSpeed, long pTimestamp) {
        id = pId;
        lon = pLon;
        lat = pLat;
        dir = pDir;
        speed = pSpeed;
        timestamp = pTimestamp;
    }
    String getId() {
        return id;
    }
    double getLon() {
        return lon;
    }
    double getLat() {
        return lat;
    }
    int getDir() {
        return dir;
    }
    double getSpeed() {
        return speed;
    }
    long getTimestamp() {
        return timestamp;
    }
    void setDistance (double pMeters) {
        meters = pMeters;
    }

    public double getDistance() {
        return meters;
    }


}

class AnzeigenData {
    String id;
    String linie;
    String ziel;
    AnzeigenData(String pId, String pLinie, String pZiel) {
        id = pId;
        linie = pLinie;
        ziel = pZiel;
    }

    public String getId() {
        return id;
    }

    public String getLinie() {
        return linie;
    }

    public String getZiel(){
        return ziel.replaceAll("~", "ẞ");
    }
}
