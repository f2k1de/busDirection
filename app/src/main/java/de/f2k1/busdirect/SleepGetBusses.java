package de.f2k1.busdirect;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

class SleepGetBusses extends Thread{
    private static List<PositionsData> l = new ArrayList<>();
    private static List<AnzeigenData> anzeigenDataArrayList = new ArrayList<>();
    private static List<String> deadBusses = new ArrayList<>();

    private Activity context;
    SleepGetBusses(Activity context) {
        this.context = context;
    }
    List<PositionsData> getResults() {
        return l;
    }
    List<AnzeigenData> getAnzeigenList() {
        return anzeigenDataArrayList;
    }
    public void run() {

        try {
            while (true) {
                TextView textView = context.findViewById(R.id.mainactivityhint);
                if(MainActivity.location != null) {
                    if(textView.getVisibility() == View.VISIBLE) {
                        context.runOnUiThread(new Runnable() {
                                                  @Override
                                                  public void run() {
                                                      TextView textView = context.findViewById(R.id.mainactivityhint);
                                                      textView.setVisibility(View.GONE);
                                                  }
                                              });

                    }
                    MainActivity.doesupdate = true;
                    getAnzeigen();
                    getBusses();
                    MainActivity.doesupdate = false;
                    sleep(5000);
                } else {
                    textView.setText(R.string.waiting_gps_fix);
                    sleep(500);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAnzeigen() {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://stadtwerke.24stundenonline.de/request.php?action=getfmsdata&fields=stadtwerke_sw_aussenanzeige%2Cstadtwerke_sw_liniennummer%2Cstadtwerke_sw_kursnummer&deviceid=217796%2C217797%2C217790%2C217765%2C217800%2C217808%2C217810%2C217777%2C217802%2C217778%2C217809%2C217813%2C217804%2C217772%2C217776%2C217771%2C217783%2C217801%2C217795%2C217811%2C217779%2C217775%2C217787%2C217814%2C217764%2C217799%2C217781%2C217763%2C217782%2C217786%2C217815%2C217806%2C217816%2C217803%2C217792%2C217762%2C217791%2C217769%2C217770%2C217805%2C217812%2C217767%2C217794%2C217774%2C217807%2C217793%2C217789%2C217773%2C217766%2C217780%2C217788%2C217798%2C217768%2C217784%2C217785&_=1586519801628")
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                String res = response.body().string();

                try {
                    JSONObject reader = new JSONObject(res);
                    JSONObject c = reader.getJSONObject("fields");
                    Iterator<String> keys = c.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        String aussenanzeige = c.getJSONObject(key).getString("stadtwerke_sw_aussenanzeige");
                        String liniennummer = c.getJSONObject(key).getString("stadtwerke_sw_liniennummer");
                        anzeigenDataArrayList.add(new AnzeigenData(key, liniennummer, aussenanzeige));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void getBusses() {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://stadtwerke.24stundenonline.de/request.php?action=getpositions")
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String res = response.body().string();
                Log.d("MA", res);

                try {
                    JSONArray reader = new JSONArray(res);
                    Log.d("MA", String.valueOf(reader.length()));

                    for(int i = 0; i < reader.length(); i++) {
                        JSONObject c = reader.getJSONObject(i);
                        String id = c.getString("id");
                        double lon = c.getDouble("WE");
                        double lat = c.getDouble("NS");
                        int dir = c.getInt("dir");
                        double speed = c.getDouble("speed");
                        long timestamp = c.getLong("unix_ts");


                        if(l.size() < i) {
                            l.add(new PositionsData(id,lon,lat,dir,speed,timestamp));
                        } else {
                            for(int k = 0; k < l.size(); k++) {
                                if(l.get(k).getId().equals(id)){
                                    l.remove(k);
                                    l.add(k, new PositionsData(id,lon,lat,dir,speed,timestamp));

                                    Log.d("added", String.valueOf(k));
                                }
                            }
                        }


                        for(int j = 0; j < l.size(); j++) {
                            float distance = getMeters(l.get(j).getLon(), l.get(j).getLat(), MainActivity.location.getLongitude(), MainActivity.location.getLatitude());
                            l.get(j).setDistance(distance);
                        }
                            Collections.sort(l, new Comparator<PositionsData>() {
                            @Override
                            public int compare(PositionsData positionsData, PositionsData t1) {
                                return (int) (positionsData.getDistance() * 10000 - t1.getDistance() * 10000);
                            }
                        });

                    }
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.adapter.notifyDataSetChanged();

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });
    }

    private float getMeters(double lon1, double lat1, double lon2, double lat2) {
        float[] result =  new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, result);
        return result[0];
    }
}
