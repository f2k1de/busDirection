package de.f2k1.busdirect;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;

import godau.fynn.librariesdirect.AboutLibrariesActivity;
import godau.fynn.librariesdirect.Library;
import godau.fynn.librariesdirect.License;


public class MainActivity extends AppCompatActivity {
    private SleepGetBusses getBusses = new SleepGetBusses(this);
    static MyRecyclerViewAdapter adapter;
    static Location location;
    static boolean doesupdate;
    private LocationListener locationListener;

    @Override
    protected void onPause () {
        super.onPause();
        getBusses.interrupt();
    }

    protected void onResume() {
        super.onResume();
        if(!getBusses.isAlive()) {
            getBusses = new SleepGetBusses(this);
            getBusses.start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.about:

                Intent intent = new AboutLibrariesActivity.IntentBuilder(this)
                        .setHeaderText(getString(R.string.about_header))
                        .setLibraries(new Library[]{
                                new Library("OkHttp", License.APACHE_20_LICENSE, null, "Square, Inc.", "https://square.github.io/okhttp/"),
                                new Library("librariesDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),
                        })
                        .build();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getBusses.start();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        adapter = new MyRecyclerViewAdapter(MainActivity.this, getBusses.getResults(), getBusses.getAnzeigenList());
        adapter.setClickListener(new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                BusDetails busDetails = new BusDetails();
                TextView tv = view.findViewById(R.id.tvBusId);
                String text = (String) tv.getText();
                String betreiber = busDetails.getBusBetrieberById(text);
                String nummer = busDetails.getBusnumberById(text);
                String kennzeichen = busDetails.getKennzeichenById(text);
                String fahrzeug = busDetails.getFahrzeugById(text);
                Toast.makeText(MainActivity.this, "Betreiber:" + betreiber + "\n" + "Fahrzeug:" + fahrzeug + "\n" + "Nummer:" + nummer + "\n" + "Kennzeichen:" + kennzeichen + "\n" + "GPSaugeID: " + text, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(adapter);


        locationListener = new LocationListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onLocationChanged(Location location) {
                Log.d("location", location.getLatitude() + "" + location.getLongitude() + "" + location.getBearing());
                //LinearLayout linearLayout = findViewById(R.id.linearLayout);
                if(!doesupdate) {
                    if(getBusses.getResults() != null) {

                        for(int i = 0; i < getBusses.getResults().size(); i++) {
                            float distance = getMeters(getBusses.getResults().get(i).getLon(), getBusses.getResults().get(i).getLat(), location.getLongitude(), location.getLatitude());
                            getBusses.getResults().get(i).setDistance(distance);
                        }
                        getBusses.getResults();

                        Collections.sort(getBusses.getResults(), new Comparator<PositionsData>() {
                            @Override
                            public int compare(PositionsData positionsData, PositionsData t1) {
                                return (int) (positionsData.getDistance() * 10000 - t1.getDistance() * 10000);
                            }
                        });

                        MainActivity.location = location;
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.d("status",s);
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("enabled",s);
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.d("disabled",s);
            }
        };

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // Permission not granted; request
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            // Permission already granted
            LocationManager locationManager;
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates("fused", // LocationManager.FUSED_PROVIDER
                    0, 0, locationListener);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "GPS Permission Granted", Toast.LENGTH_SHORT).show();

                        // Request location updates after permission has been granted
                        LocationManager locationManager;
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        locationManager.requestLocationUpdates("fused", 0, 0, locationListener);
                    }
                } else {
                    Toast.makeText(this, "GPS Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    private float getMeters(double lon1, double lat1, double lon2, double lat2) {
        float[] result =  new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, result);
        return result[0];
    }
}
