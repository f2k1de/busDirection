package de.f2k1.busdirect;

import java.util.HashMap;
import java.util.Map;

public class BusDetails {
    private Map<String, String[]> map = new HashMap<>();
    private static final String TEXT = "217796;50;Stadt-SW;SW-LP 50;VDL ALE\n" +
            "217797;69;Stadt-SW;SW-SU 69;C2 Gelenk\n" +
            "217790;91;Kleinhenz;GEO-LK 130;NL 323\n" +
            "217765;94;Kleinhenz;GEO-LK 430;O 530 G\n" +
            "217800;60;Stadt-SW;SW-AC 60;VDL ALE\n" +
            "217808;61;Stadt-SW;SW-AD 61;VDL ALE\n" +
            "217810;56;Stadt-SW;SW-AE 56;Urbino 12\n" +
            "217777;54;Stadt-SW;SW-CA 54;Urbino 12\n" +
            "217802;57;Stadt-SW;SW-CC 57;Urbino 12\n" +
            "217778;55;Stadt-SW;SW-DT 55;Urbino 12\n" +
            "217809;97;Federlein;SW-GX 200;C2\n" +
            "217813;98;Federlein;SW-GX 4000;Urbino 12\n" +
            "217804;??;217804;;\n" +
            "217772;96;Federlein;SW-GX 9000;MAN NG 363 (Gelenk)\n" +
            "217776;92;Kleinhenz;SW-LK 1530;MAN NL 283\n" +
            "217771;93;Kleinhenz;SW-LK 1630;O 530 II\n" +
            "217783;58;Stadt-SW;SW-NL 58;VDL ALE\n" +
            "217801;59;Stadt-SW;SW-NL 59;VDL ALE\n" +
            "217795;71;Stadt-SW;SW-S 1471;C2 Gelenk\n" +
            "217811;72;Stadt-SW;SW-S 1472;C2 Gelenk\n" +
            "217779;73;Stadt-SW;SW-S 1473;C2 Gelenk\n" +
            "217775;10;Stadt-SW;SW-S 1510;C2\n" +
            "217787;11;Stadt-SW;SW-S 1511;C2\n" +
            "217814;74;Stadt-SW;SW-S 1574;C2\n" +
            "217764;75;Stadt-SW;SW-S 1575;C2\n" +
            "217799;76;Stadt-SW;SW-S 1576;C2\n" +
            "217781;77;Stadt-SW;SW-S 1677;C2\n" +
            "217763;78;Stadt-SW;SW-S 1678;C2\n" +
            "217782;13;Stadt-SW;SW-S 1813;C2\n" +
            "217786;14;Stadt-SW;SW-S 1814;C2\n" +
            "217815;15;Stadt-SW;SW-S 1815;C2\n" +
            "217806;16;Stadt-SW;SW-S 1916;C2\n" +
            "217816;17;Stadt-SW;SW-S 1917;C2\n" +
            "217803;18;Stadt-SW;SW-S 1918;C2\n" +
            "217792;68;Stadt-SW;SW-SD 68;C2 Gelenk\n" +
            "217762;E;Ersatz im BTH;;\n" +
            "217791;64;Stadt-SW;SW-ST 64;VDL ALE\n" +
            "217769;67;Stadt-SW;SW-ST 67;C2 Gelenk\n" +
            "217770;70;Stadt-SW;SW-SY 70;C2 Gelenk\n" +
            "217805;85;Metz Ludwig;SW-TM 905 ;NG 353 LC Gelenk\n" +
            "217812;88;Metz Ludwig;SW-TM 906;NL 283 LC\n" +
            "217767;86;Metz Ludwig;SW-TM 908;NG 353 LC Gelenk\n" +
            "217794;87;Metz Ludwig;SW-TM 921;NL 283 LC\n" +
            "217774;89;Metz Ludwig;SW-TM 923;NL 323 LC\n" +
            "217807;65;Stadt-SW;SW-TR 65;VDL ALE\n" +
            "217793;66;Stadt-SW;SW-TR 66;VDL ALE\n" +
            "217789;62;Stadt-SW;SW-ST 62;VDL ALE\n" +
            "217773;63;Stadt-SW;SW-TS 63;VDL ALE\n" +
            "217766;51;Stadt-SW;SW-TT 51;VDL ALE\n" +
            "217780;52;Stadt-SW;SW-TT 52;VDL ALE\n" +
            "217788;53;Stadt-SW;SW-TT 53;VDL ALE\n" +
            "217798;79;Stadt-SW;SW-V 1679;C2 Gelenk\n" +
            "217768;12;Stadt-SW;SW-V 1812;C2\n" +
            "217784;19;Stadt-SW;SW-V 1919;C2\n" +
            "217785;E;Ersatz im Bth;;";


    BusDetails() {
        parseCSV();
    }

    String getBusnumberById(String pId) {
        if (map.containsKey(pId)) {
            return map.get(pId)[1];
        } else return null;
    }

    String getBusBetreiberByNummer(String pNummer) {
        for (String[] entry : map.values()) {
            if (entry[1].equals(pNummer)) {
                return entry[2];
            }
        }
        return null;
    }

    boolean isStadtbus(String pId) {
        String betreiber = getBusBetrieberById(pId);
        return "Stadt-SW".equals(betreiber);
    }

    String getBusBetrieberById(String pId) {
        if (map.containsKey(pId)) {
            return map.get(pId)[2];
        } else return null;
    }

    String getKennzeichenById(String pId) {
        if (map.containsKey(pId)) {
            return map.get(pId)[3];
        } else return null;
    }

    String getFahrzeugById(String pId) {
        if (map.containsKey(pId)) {
            return map.get(pId)[4];
        } else return null;
    }

    private void parseCSV() {
        String[] newtext = TEXT.split("\n");
        for (String txt: newtext) {
            String[] tmp = txt.split(";");
            map.put(tmp[0], tmp);
        }
    }
}
